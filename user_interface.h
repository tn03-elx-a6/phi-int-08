
/** 
 * Description: The function is used to render background on the screen. 
 * Input Arguments: color_name - It is a character array containing the name of the color to be displayed, starting_location_on_screen - It is a integer value defining the exact postion at which the background must start to be displayed.
 * Output: -
 */
void render_background(char *color_name, int starting_location_onscreen);

/** 
 * Description: The function is used to render rectangular pane on the screen. 
 * Input Arguments: shape_path - It is integer value containing the address of the shape to be displayed, shape_size - It is the integer value having size of the shape to be dsplayed on screen, starting_location_on_screen - It is a integer value defining the exact postion at which the shape must start to be displayed.
 * Output: - 
 */
void render_rectangular_pane(char *shape_path, int shape_size, int starting_location_onscreen);

/** 
 * Description: The function is used to display text on the screen.
 * Input Arguments: text - The character array f the text to be displayed, starting_location_on_screen - It is a integer value defining th exact postion at which the text must start to be displayed, font_size - It is the integer value having size of the font to be dsplayed on screen, font_color - It is the character array containing the color of the font to be displayed, background_color - It is the background color of the text to be displayed.
 * Output: -
 */
void render_text(char *text, int starting_location_onscreen, int font_size, char *font_color, char *background_color);

/** 
 * Description: The function is used to display current date on the screen.
 * Input Arguments: date - It is a structure containing members like date, month and year,  starting_location_on_screen - It is a integer value defining th exact postion at which the date must start to be displayed, font_size - It is the integer value having size of the font to be dsplayed on screen, font_color - It is the character array containing the color of the font to be displayed, background_color - It is the background color of the text to be displayed.
 * Output: -
 */
void render_current_date(struct date d, int starting_location_onscreen, int font_size, char *font_color, char *background_color);

/** 
 * Description: The function is used to display current time on the screen.
 * Input Arguments: date - It is a structure containing members like hours, minutes and seconds,  starting_location_on_screen - It is a integer value defining th exact postion at which the time must start to be displayed, font_size - It is the integer value having size of the font to be dsplayed on screen, font_color - It is the character array containing the color of the font to be displayed, background_color - It is the background color of the text to be displayed.
 * Output: -
 */
void render_current_time(struct time t, int starting_location, int font_size, char *font_color, char *background_color);

