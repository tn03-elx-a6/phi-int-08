
/** 
 * Description: The function is used to create a template.
 * Input Arguments: -
 * Output: The function returns a structure containing elements in the template.
 */
struct template create_template();

/** 
 * Description: The function is used to add background color to the template.
 * Input Arguments: -
 * Output: -
 */
void add_background_color_to_template();

/** 
 * Description: The function is used to add rectangular pane to the template.
 * Input Arguments: -
 * Output: -
 */
void add_rectangular_pane_to_template();

/** 
 * Description: The function is used to add current date to the template.
 * Input Arguments: -
 * Output: -
 */
void add_current_date_to_template();

/** 
 * Description: The function is used to add current time to the template.
 * Input Arguments: -
 * Output: -
 */
void add_current_time_to_template();

/** 
 * Description: The function is used to add text to the template.
 * Input Arguments: -
 * Output: -
 */
void add_text_to_template();

/** 
 * Description: The function is used to add next line to the template.
 * Input Arguments: -
 * Output: -
 */
void add_next_line_to_template();

/** 
 * Description: The function is used to render template on the screen.
 * Input Arguments: It intakes the structure called template containing various members like background color, rectangular pane, current date, current time, text, next line.
 * Output: -
 */
void render_template(struct template);