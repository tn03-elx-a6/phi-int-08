
/** 
 * Description: This function will initialize the storage location for the parameter. 
 * Input Arguments: -
 * Output: The function will return the storage location at which the parameter value can be stored.
 */
int init_parameter_storage_location();

/** 
 * Description: The function is used to add new parameter in the system.
 * Input Arguments: The function will intake the old sturcture and a character array that is the name of the new parameter value to be stored.
 * Output: The function will return a new struture containing all the members of the old structure and the newly added member.
 */
struct parameter q add_new_parameter(struct parameter p, char *parameter_name);

/** 
 * Description: The function is used to set values for the different parameters.
 * Input Arguments: The function will intake structure called parameter for which values need to be assigned for each member.
 * Output: -
 */
void set_parameter_value(struct parameter p);

/** 
 * Description: This function is used to check if the value of the respective parameter has valid data type.  
 * Input Arguments: parameter p - Structure called parameter is given as input argument containing memebers like parameter name and parameter value.
 * Output: For all parameter names if thier respective values have required data type, the function returns 1, else 0.
 */
int check_parameter_datatype(struct parameter p);

/** 
 * Description: The function is used to get all parameter names and their values as assigned by the user. 
 * Input Arguments: -
 * Output: The function returns structure containing all the parameter names and thier respective values.
 */
struct parameter get_parameter_value();
