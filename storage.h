
/** 
 * Description: The function is used to initialize the storage location..
 * Input Arguments: block_no - It is the integer value of the block number at which the data needs to be accessed, block_size - It is the size of the memory block.
 * Output: -
 */
void init_storage(int block_no, int block_size);

/** 
 * Description: The function is used to read data from the storage.
 * Input Arguments: patameter_value - It is the address of the parameter value that needs to be read.
 * Output: It is an integer called parameter value.
 */
int read_byte(int *parameter_value);

/** 
 * Description: The function is used to write values to the storage.
 * Input Arguments: paramter_value - It is the value of the paramter that needs to be stored.
 * Output: It is the memory location of the parameter value that is stored in the system.
 */
int *write_byte(int parameter_value);