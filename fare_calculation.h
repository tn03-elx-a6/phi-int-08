
/** 
 * Description: The function is used to calculate the total cost of the journey.
 * Input Arguments: no_adult - Integer value of total number of adults travelling, no_child - Integer value of total number of children traveling, rate_adult - Integer value of rate of travel per km for an adult, rate_child - Integer value of rate of travel per km for a child, dist - Float value of the total distance to be travelled in kms.  
 * Output: The output of the function is a floating value which is the total fare for the journey.
 */
float calculateFare(int no_adult, int no_child, int rate_adult, int rate_child, float dist);