
/** 
 * Description: This function is used for admin authentication into the system. 
 * Input Arguments: user_id - A character array containing unique id of the user, pass - A character array containing 8 character password.
 * Output: The function will return an integer - if it returns 0 the authentication is unsuccessful else if it returns 1 the authentcation is successful.
 */
int admin_authentication(char *user_id,char *pass);
