
/** 
 * Description: The function is used to get the current date.
 * Input Arguments: -
 * Output: The function returns a structure called date containing members like date, month and year.
 */
struct date get_date();

/** 
 * Description: The function is used to get the current date
 * Input Arguments: -
 * Output: The function returns a structure called time containing members like hours, minutes and seconds.
 */
struct time get_time();