
/** 
 * Description: The function is used to initialize printer for usage.
 * Input Arguments: -
 * Output: If printer is initialized and ready to be used the function returns 1, else 0.
 */
int initialize_printer();

/** 
 * Description: The function is used to pass print data and its size to the printer.
 * Input Arguments: data_to_print - It is the character array of the data to be printed, size_of_data - It is the integer value of the size of the data to be printed.
 * Output: The function returns a structure containing the members like data to be printed and the size of data to be printed.
 */
struct print_data print(char *data_to_print, int size_of_data);