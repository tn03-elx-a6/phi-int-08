
/** 
 * Description: The function is used to initialize printer for usage.
 * Input Arguments: -
 * Output: If printer is initialized and ready to be used the function returns 1, else 0.
 */
int initialize_printer();

/** 
 * Description: The function is used to check if printer is connected.
 * Input Arguments: -
 * Output: If printer is connected the function returns 1, else 0.
 */
int check_printer_if_connected();

/** 
 * Description: The function is used to check if printer loaded with paper.
 * Input Arguments: -
 * Output: If printer is loaded with paper the function returns 1, else 0.
 */
int check_if_printer_loaded_with_paper();

/** 
 * Description: The function is used to check if printer is free for printing and currently no print is being executed.
 * Input Arguments: -
 * Output: If printer is free the function returns 1, else 0.
 */
int check_if_printer_free_for_printing();

/** 
 * Description: The function is used create a format and arranging the data before printing.
 * Input Arguments: -
 * Output: -
 */
void create_print_template();

/** 
 * Description: The function is used to give print command to the printer.
 * Input Arguments: -
 * Output: -
 */
void print();

